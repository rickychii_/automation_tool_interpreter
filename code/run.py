#!/usr/bin/python3

#imports
import sys
import pyautogui
import datetime
from time import sleep



class autoOperations:
    REPEAT='repeat'
    LEFT_CLICK='leftclick'
    RIGHT_CLICK='rightclick'
    MIDDLE_CLICK='middleclick'
    LEFT_HOLD='lefthold'
    RIGHT_HOLD='righthold'
    MIDDLE_HOLD='middlehold'
    END_HOLD='endhold'
    GOTO='goto'

    def NOW(self):
        ct = datetime.datetime.now() 
        return ct


def mainLoop():
    for line in lines:
        l = line.strip()
        if(not l): continue
        if(not l[5]): continue
        if(l[0]=='#'): continue
        if(l==const.LEFT_CLICK):
            pyautogui.click(button='left')
        elif(l==const.RIGHT_CLICK):
            pyautogui.click(button='right')
        elif(l==const.MIDDLE_CLICK):
            pyautogui.click(button='middle')
        elif(l==const.LEFT_HOLD):
            pyautogui.mouseDown(button='left')
        elif(l==const.RIGHT_HOLD):
            pyautogui.mouseDown(button='right')
        elif(l==const.MIDDLE_HOLD):
            pyautogui.mouseDown(button='middle')
        elif(l==const.END_HOLD):
            pyautogui.mouseUp(button='left')
            pyautogui.mouseUp(button='right')
            pyautogui.mouseUp(button='middle')
        elif(l[0:4]==const.GOTO):
            x = l.replace(const.GOTO,"")
            x = x.split(',')
            x1 = int(x[0])
            y1 = int(x[1])
            pyautogui.moveTo(x1,y1)
        sleep(0.065)


#read and validate input
paramenters = sys.argv
if(len(paramenters) == 1):
    print("Not enough parameters!")
    sys.exit()
try:
    file = paramenters[1]
    file1 = open(file,'r')
    lines = file1.readlines()
    const = autoOperations()
    count = 0
    howmany = 0
    allok = True
    for line in lines:
        l = line.strip()
        if(not l): continue
        if(not l[5]): raise Exception('')
        if(l[0]=='#'): continue
        if(count == 0):
            if(not(l[0:6]==const.REPEAT)):
                allok = False
                raise Exception('')
            else:
                foundHeader = True
                count = count + 1
                x = l.replace(const.REPEAT+"=","")
                howmany = int(x)
                continue
        count = count + 1
        if(l==const.LEFT_CLICK): 
            allok = True
        elif(l==const.RIGHT_CLICK):
            allok = True
        elif(l==const.MIDDLE_CLICK):
            allok = True
        elif(l==const.LEFT_HOLD):
            allok = True
        elif(l==const.RIGHT_HOLD):
            allok = True
        elif(l==const.MIDDLE_HOLD):
            allok = True
        elif(l==const.END_HOLD):
            allok = True
        elif(l[0:4]==const.GOTO):
            x = l.replace(const.GOTO,"")
            x = x.split(',')
            x1 = int(x[0])
            y1 = int(x[1])
            allok = True
        else:
            allok = False 
            raise Exception('')

    if(allok==True):
        print("starting elaboration..", const.NOW())
        if(howmany == 0):
            while(True): mainLoop()
        else:
            x = range(howmany)
            for n in x:
                print("elaboration ",(n+1),const.NOW())
                mainLoop()
            print("end elaboration..", const.NOW())
        

except KeyboardInterrupt:
    print("end elaboration..", const.NOW())
    pass

except Exception as x:
    print("File does not exist or not well formatted!")
    print(x)
    sys.exit()