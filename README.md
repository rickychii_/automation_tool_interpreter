# automation_tool_interpreter

A quick python intrpreter for automation task like mouse move, mouse click, mouse hold click.
For use the tool there's a simple script syntax for edit what automation task will do, also there's possibility to repet task N times or do it in infinite loop.

For run program need to type in terminal: python3 run.py [script name]

[script name] need to be a name of a valid text plain file, which contains automated instructions for teach to program what to do.

You can find an example of this file in code/batch